package com.springstarter.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springstarter.app.dao.CommentaireRepository;
import com.springstarter.app.model.Commentaire;

@RestController
public class CommentaireRestController {
	
	@Autowired
	private CommentaireRepository commentaireRepository;
	
	@GetMapping("/commentaires")
	public List<Commentaire> getCommentaire(){
		return commentaireRepository.findAll();
	}
	
	@GetMapping("/commentaires/{id}")
	public Commentaire getCommentaireById(@PathVariable Integer id) {
		return commentaireRepository.findById(id).get();
	}
	
	@PutMapping("/commentaires/{id}")
	public Commentaire modifCommentaire(@PathVariable Integer id, @RequestBody Commentaire c) {
		c.setId(id);
		return commentaireRepository.save(c);
	}
	
	@DeleteMapping("/commentaires/{id}")
	public boolean supprCommentaire(@PathVariable Integer id, @RequestBody Commentaire c) {
		c.setId(id);
		commentaireRepository.deleteById(id);
		return  true;
	}
	
	@PostMapping("/commentaires")
	public Commentaire save(@RequestBody Commentaire c) {
		return commentaireRepository.save(c);
	}
}
