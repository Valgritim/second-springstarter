package com.springstarter.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondSpringstarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecondSpringstarterApplication.class, args);
	}

}
