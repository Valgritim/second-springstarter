package com.springstarter.app.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Commentaire implements Serializable{
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
		private String libelle;
		private String sujet;
		private String description;
		private String date;
		
		public Commentaire(String libelle, String sujet, String description, String date) {
			super();
			this.libelle = libelle;
			this.sujet = sujet;
			this.description = description;
			this.date = date;
		}
		public Commentaire() {
			super();
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getLibelle() {
			return libelle;
		}
		public void setLibelle(String libelle) {
			this.libelle = libelle;
		}
		public String getSujet() {
			return sujet;
		}
		public void setSujet(String sujet) {
			this.sujet = sujet;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		@Override
		public String toString() {
			return "Commentaire [id=" + id + ", libelle=" + libelle + ", sujet=" + sujet + ", description="
					+ description + ", date=" + date + "]";
		}
		
		
		
		
}
