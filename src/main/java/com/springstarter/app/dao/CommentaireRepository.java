package com.springstarter.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springstarter.app.model.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Integer>{

}
